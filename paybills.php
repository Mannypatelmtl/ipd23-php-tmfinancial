<?php
 
// transfer funds 
require_once 'vendor/autoload.php';
 
require_once 'init.php';
 
// STATE 1: first display of the form
 
// STATE 1: first display of the form
$app->get('/paybill', function ($request, $response, $args) {
    $clientId = $_SESSION['user']['id'];
    $fromAccount = DB::query("SELECT * FROM accounts where clientId=%d", $clientId);
    $toExternal = DB::query("SELECT * FROM externals where clientId=%d", $clientId);
    return $this->view->render($response, 'paybills.html.twig', ['f' => $fromAccount,'t' =>$toExternal]);

});
// STATE 2&3: receiving submission
$app->post('/paybill', function ($request, $response, $args) use ($log) {
    $clientId = $_SESSION['user']['id'];    
    $fromAccount = $request->getParam('fromAccount');
    $toExternal = $request->getParam('toExternal');
    $payAmount = $request->getParam('payAmount');
    $from = DB::query("SELECT * FROM accounts where clientId=%d", $clientId);
    $to = DB::query("SELECT * FROM externals where clientId=%d", $clientId);
    $errorList = [];
    $data = DB::queryFirstRow("SELECT * from accounts WHERE id=%d", $fromAccount);    
    if ($data['balance'] < $payAmount) {
        $errorList[] = "You don't have sufficient funds to transfer the amount entered";        
    } 
    if ($errorList) { // STATE 2: errors - redisplay the form
        return $this->view->render($response, 'paybills.html.twig', ['errorList' => $errorList, 'f' => $from, 't' => $to]);
    } else { // STATE 3: success

        $accountFrom = DB::queryFirstRow("SELECT * FROM accounts WHERE id=%s", $fromAccount);
        $external = DB::queryFirstRow("SELECT * FROM externals WHERE id=%s", $toExternal);
        $description = " Bill Payment  $external[name] | $external[referenceNo]";
    
        $newBalanceFrom = $accountFrom['balance'] - $payAmount;
        DB::update('accounts', ['balance' => $newBalanceFrom], "id=%d", $accountFrom['id']); 
        $log->debug(sprintf("balance debited from account", DB::insertId(), $_SESSION['user']['id'], $_SERVER['REMOTE_ADDR']));

        $data = ['description' => $description, 'fromAccount' => $accountFrom['id'], 'toExternal' => $external['id'], 'amount' => $payAmount];
        DB::insert('transactions', $data); 
        $log->debug(sprintf("transaction created", DB::insertId(), $_SESSION['user']['id'], $_SERVER['REMOTE_ADDR']));

        return $this->view->render($response, 'paybills_success.html.twig');
    }
});
 
