<?php

require_once 'vendor/autoload.php';
require_once 'init.php';
require_once 'validations.php';

// -- client list  -----------------------------------------------------
$app->get('/admin/clients/list', function ($request, $response, $args) {
    $clientsList = DB::query("SELECT * FROM clients");
    return $this->view->render($response, 'admin/clients_list.html.twig', ['clientsList' => $clientsList]);
});

// -- client edit --------------------------------------------------
// STATE 1: first display
$app->get('/admin/clients/edit/{id:[0-9]+}', function ($request, $response, $args) {
    $client = DB::queryFirstRow("SELECT * FROM clients WHERE id=%d", $args['id']);
    if (!$client) {
        $response = $response->withStatus(404);
        return $this->view->render($response, 'admin/not_found.html.twig');
    }

    return $this->view->render($response, 'admin/clients_edit.html.twig', ['v' => $client]);
});

// STATE 2 & 3: receiving submission 
$app->post('/admin/clients/edit/{id:[0-9]+}', function ($request, $response, $args) use ($log) {
    $isAdmin = $request->getParam('isAdmin') ?? '0';
    $name = $request->getParam('name');
    $address = $request->getParam('address');
    $email = $request->getParam('email');
    $phone = $request->getParam('phone');
    $dob = $request->getParam('dob');
    $password = $request->getParam('password');
    $password2 = $request->getParam('password2');
    $id = $args['id'];

    $errorList = [];
    if (($result = validateName($name)) !== true) { $errorList[] = $result; }
    if (($result = validateAddress($address)) !== true) { $errorList[] = $result; }
    if (($result = validateEmail($email, $id)) !== true) { $errorList[] = $result; }
    if (($result = validatePhone($phone)) !== true) { $errorList[] = $result; }
    if ($password != "") {
        if (($result = validatePassword($password, $password2)) !== true) { $errorList[] = $result; }
    }
    if ($errorList) { // STATE 2: errors - redisplay the form   
        $valuesList = ['isAdmin' => $isAdmin, 'name' => $name, 'address' => $address, 'email' => $email, 'phone' => $phone, 'dob' => $dob];     
        return $this->view->render($response, 'admin/clients_edit.html.twig', ['errorList' => $errorList, 'v' => $valuesList]);
    } else { // STATE 3: success
        $data = [ 'isAdmin' => $isAdmin, 'name' => $name, 'address' => $address, 'email' => $email, 'phone' => $phone, 'dob' => $dob];
        if ($password != "") {            
            $data['password'] = $password;
        }
        DB::update('clients', $data, "id=%d", $args['id']);
        $log->debug(sprintf("Client profile updated with Id=%s", $args['id'], $_SESSION['user']['id'], $_SERVER['REMOTE_ADDR']));
        setFlashMessage("Client profile updated successfully"); 
        return $response->withStatus(302)->withHeader('Location', '/admin/clients/list');
    }
}); 

// -- client add --------------------------------------------------
// STATE 1: first display
$app->get('/admin/clients/add', function ($request, $response, $args) {
    return $this->view->render($response, 'admin/clients_add.html.twig');
});

// STATE 2 & 3: receiving submission 
$app->post('/admin/clients/add', function ($request, $response, $args) use ($log) {
    $isAdmin = $request->getParam('isAdmin') ?? '0';
    $name = $request->getParam('name');
    $address = $request->getParam('address');
    $email = $request->getParam('email');
    $phone = $request->getParam('phone');
    $dob = $request->getParam('dob');
    $password = $request->getParam('password');
    $password2 = $request->getParam('password2'); 

    $errorList = [];
    if (($result = validateName($name)) !== true) { $errorList[] = $result; }
    if (($result = validateAddress($address)) !== true) { $errorList[] = $result; }
    if (($result = validateEmail($email)) !== true) { $errorList[] = $result; }
    if (($result = validatePhone($phone)) !== true) { $errorList[] = $result; }
    if (($result = validatePassword($password, $password2)) !== true) { $errorList[] = $result; }

    if ($errorList) { // STATE 2: errors - redisplay the form 
        $valuesList = ['isAdmin' => $isAdmin, 'name' => $name, 'address' => $address, 'email' => $email, 'phone' => $phone, 'dob' => $dob];           
        return $this->view->render($response, 'admin/clients_add.html.twig', ['errorList' => $errorList, 'v' => $valuesList]);
    } else { // STATE 3: success
        $data = [ 'isAdmin' => $isAdmin, 'name' => $name, 'address' => $address, 'email' => $email, 
                    'phone' => $phone, 'dob' => $dob, 'password' => $password];
        DB::insert('clients', $data); 
        $log->debug(sprintf("New client profile created  with Id=%s", DB::insertId(), $_SESSION['user']['id'], $_SERVER['REMOTE_ADDR']));
        setFlashMessage("Client registered successfully");
        return $response->withStatus(302)->withHeader('Location', '/admin/clients/list');
    }
}); 

// -- accounts list ------------------------------------------------------
$app->get('/admin/accounts/list/{id:[0-9]+}', function ($request, $response, $args) {
    $client = DB::queryFirstRow("SELECT * FROM clients WHERE id=%d", $args['id']);
    $accountsList = DB::query("SELECT * FROM accounts WHERE clientId=%d", $args['id']);
    return $this->view->render($response, 'admin/accounts_list.html.twig', ['accountsList' => $accountsList, 'client' => $client]);
});

// -- new account --------------------------------------------------------
$app->get('/admin/accounts/create/{id:[0-9]+}', function ($request, $response, $args) {
    $client = DB::queryFirstRow("SELECT * FROM clients WHERE id=%d", $args['id']);    
    return $this->view->render($response, 'admin/accounts_create.html.twig', ['client' => $client]);
});

$app->post('/admin/accounts/create/{id:[0-9]+}', function ($request, $response, $args) {
    $accountType = $request->getParam('accountType');
    $today = date("Ymd");
    $data = ['clientId' => $args['id'], 'accountType' => $accountType, 'dateOpened' => $today, 'balance' => 0, 'interestRate' => 0.1];
    DB::insert('accounts', $data);
    setFlashMessage("Client's account created successfully");
    return $response->withStatus(302)->withHeader('Location', '/admin/accounts/list/' . $args['id']);
});

// -- transactions list --------------------------------------------------
$app->get('/admin/transactions/list/{id:[0-9]+}', function ($request, $response, $args) {
    $transactionsList = DB::query("SELECT * FROM transactions WHERE toAccount=%d OR fromAccount=%d", $args['id'], $args['id']);
    $data = DB::queryFirstRow("SELECT c.name FROM accounts as a INNER JOIN clients as c WHERE a.clientId=c.id AND a.id=%s", $args['id']);
    $name = $data['name'];    
    return $this->view->render($response, 'admin/transactions_list.html.twig', ['t' => $transactionsList, 'name' => $name, 'accountId' => $args['id']]);
});

