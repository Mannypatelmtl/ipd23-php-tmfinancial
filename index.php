<?php

session_start();

require_once 'vendor/autoload.php';

require_once 'init.php';
require_once 'validations.php';

// Define app routes below
require_once 'admin.php';
require_once 'clients.php';
require_once 'listaccounts.php';
require_once 'transferfunds.php';
require_once 'paybills.php';
require_once 'depositcheque.php';
require_once 'listtransactions.php';
require_once 'createaccount.php';

// Run app - must be the last operation
$app->run();