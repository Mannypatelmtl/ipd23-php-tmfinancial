<?php
 
// transfer funds 
require_once 'vendor/autoload.php';
 
require_once 'init.php';
 
// STATE 1: first display of the form
 
// STATE 1: first display of the form
$app->get('/transferfunds', function ($request, $response, $args) {
    $clientId = $_SESSION['user']['id'];
    $fromAccount = DB::query("SELECT * FROM accounts where clientId=%d", $clientId);
    $toAccount = DB::query("SELECT * FROM accounts where clientId=%d", $clientId);
    return $this->view->render($response, 'transferfunds.html.twig', ['f' => $fromAccount,'t' =>$toAccount]);
});
 
// STATE 2&3: receiving submission
$app->post('/transferfunds', function ($request, $response, $args) use ($log) {
    $clientId = $_SESSION['user']['id'];    
    $fromAccount = $request->getParam('fromAccount');
    $toAccount = $request->getParam('toAccount');
    $transferAmount = $request->getParam('transferAmount');
    $description  = "Funds transfered from  $fromAccount to $toAccount" ;
    $from = DB::query("SELECT * FROM accounts where clientId=%d", $clientId);
    $to = DB::query("SELECT * FROM accounts where clientId=%d", $clientId);
 
    // need client id //
    //
    $errorList = [];
    if ($fromAccount == $toAccount) {
        $errorList[] = "From and To accounts are same";
    }  
    $clientId = $_SESSION['user']['id'];  
    $balance = DB::queryFirstRow("SELECT balance from accounts  WHERE clientId=%d AND id=%s",  $clientId, $fromAccount);    
    if ($balance < $transferAmount) {
        $errorList[] = "You dont have sufficient funds to transfer the amount Entered";
    } 
 
    if ($errorList) { // STATE 2: errors - redisplay the form
        return $this->view->render($response, 'transferfunds.html.twig', ['errorList' => $errorList, 'f' => $from,'t' =>$to]);
    } else { // STATE 3: success
 
        // create a transaction
        // update balance on fromaccount
        // update balance on toaccount
        $accountFrom = DB::queryFirstRow("SELECT * FROM accounts WHERE id=%s", $fromAccount);
        $accountTo = DB::queryFirstRow("SELECT * FROM accounts WHERE id=%s", $toAccount);
    
        $newBalanceFrom = $accountFrom['balance'] - $transferAmount;
        $newBalanceTo = $accountTo['balance'] + $transferAmount;
        DB::update('accounts', ['balance' => $newBalanceFrom], "id=%d", $accountFrom['id']); 
        $log->debug(sprintf("balance debited from account", DB::insertId(), $_SESSION['user']['id'], $_SERVER['REMOTE_ADDR']));
        DB::update('accounts', ['balance' => $newBalanceTo], "id=%d", $accountTo['id']);
        $log->debug(sprintf("balance credited to account", DB::insertId(), $_SESSION['user']['id'], $_SERVER['REMOTE_ADDR']));

        $data = ['description' => $description, 'fromAccount' => $accountFrom['id'], 'toAccount' => $accountTo['id'], 'amount' => $transferAmount];
        DB::insert('transactions', $data); 
        $log->debug(sprintf("transaction created", DB::insertId(), $_SESSION['user']['id'], $_SERVER['REMOTE_ADDR']));

        return $this->view->render($response, 'transferfunds_success.html.twig');
    }
});