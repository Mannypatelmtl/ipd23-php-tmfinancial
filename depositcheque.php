<?php

// deposit cheque
require_once 'vendor/autoload.php';

require_once 'init.php';


// STATE 1: first display of the form
$app->get('/depositcheque', function ($request, $response, $args) {
    $clientId = $_SESSION['user']['id'];
    $accountsList = DB::query("SELECT * FROM accounts where clientId=%d", $clientId);
    return $this->view->render($response, 'depositcheque.html.twig', ['a' => $accountsList]);    
});

// STATE 2&3: receiving submission
$app->post('/depositcheque', function ($request, $response, $args) use ($log) {
    $clientId = $_SESSION['user']['id'];
    $accountsList = DB::query("SELECT * FROM accounts where clientId=%d", $clientId);
    $uploadedFiles = $request->getUploadedFiles();
    $imageFront = $uploadedFiles['chqFront'];
    $imageBack = $uploadedFiles['chqBack'];
    $toAccount = $request->getParam('toAccount');
    $depositAmount = $request->getParam('transferAmount');
    $description = "Cheque deposit";

    $errorList = [];

    // verify image
    $hasPhoto1 = false;
    $mimeType1 = "";    
    if ($imageFront->getError() != UPLOAD_ERR_NO_FILE) { // was anything uploaded?
        // print_r($uploadedImage->getError());
        $hasPhoto1 = true;
        $result = verifyUploadedPhoto($imageFront, $mimeType1);
        if ($result !== TRUE) {
            $errorList[] = $result;
        } 
    } else {
        $errorList[] = "Picture of front of cheque missing";
    }
    $hasPhoto2 = false;
    $mimeType2 = "";
    if ($imageBack->getError() != UPLOAD_ERR_NO_FILE) { // was anything uploaded?
        // print_r($uploadedImage->getError());
        $hasPhoto2 = true;
        $result = verifyUploadedPhoto($imageBack, $mimeType2);
        if ($result !== TRUE) {
            $errorList[] = $result;
        } 
    } else {
        $errorList[] = "Picture of back of cheque missing";
    }
    if ($depositAmount <= 0 || $depositAmount > 10000) {
        $errorList[] = "Amount must be positive and less than $10,000.00";
    }
    
    if ($errorList) { // STATE 2: errors - redisplay the form
        return $this->view->render($response, 'depositcheque.html.twig', ['errorList' => $errorList, 'a' => $accountsList]);
    } else { // STATE 3: success
        $photoData1 = null;
        if ($hasPhoto1) { $photoData1 = file_get_contents($imageFront->file); }
        if ($hasPhoto2) { $photoData2 = file_get_contents($imageBack->file); }

        $accountId = DB::queryFirstRow("SELECT * FROM accounts WHERE id=%s", $toAccount);
        $newBalance = $accountId['balance'] + $depositAmount;
        DB::update('accounts', ['balance' => $newBalance], "id=%d", $accountId['id']); 
        $valuesList = ['toAccount' => $accountId['id'], 'description' => $description, 'amount' => $depositAmount, 'imageFront' => $photoData1, 'imageBack' => $photoData2];        
        DB::insert('transactions', $valuesList);
        $log->debug(sprintf("New client profile created  with Id=%s", DB::insertId()));
        return $this->view->render($response, 'depositcheque_success.html.twig');
    }
});

function verifyUploadedPhoto($photo, &$mime = null) {
    if ($photo->getError() != 0) {
        return "Error uploading photo " . $photo->getError();
    } 
    if ($photo->getSize() > 1024*1024) { // 1MB
        return "File too big. 1MB max is allowed.";
    }
    $info = getimagesize($photo->file);
    if (!$info) {
        return "File is not an image";
    }
    // echo "\n\nimage info\n";
    // print_r($info);
    if ($info[0] < 200 || $info[0] > 1000 || $info[1] < 200 || $info[1] > 1000) {
        return "Width and height must be within 200-1000 pixels range";
    }
    $ext = "";
    switch ($info['mime']) {
        case 'image/jpeg': $ext = "jpg"; break;
        case 'image/gif': $ext = "gif"; break;
        case 'image/png': $ext = "png"; break;
        default:
            return "Only JPG, GIF and PNG file types are allowed";
    } 
    if (!is_null($mime)) {
        $mime = $info['mime'];
    }
    return TRUE;
}