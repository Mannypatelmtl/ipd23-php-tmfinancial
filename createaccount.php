<?php

require_once 'vendor/autoload.php';

require_once 'init.php';


// STATE 1: first display of the form
$app->get('/createaccount', function ($request, $response, $args) {
    $client = $_SESSION['user'];
    return $this->view->render($response, 'createaccount.html.twig', ['client' => $client]);    
});

// STATE 2&3: receiving submission
$app->post('/createaccount', function ($request, $response, $args) use ($log) {
    $clientId = $_SESSION['user']['id'];
    $accountType = $request->getParam('accountType');
    $today = date("Ymd");
    $data = ['clientId' => $clientId, 'accountType' => $accountType, 'dateOpened' => $today, 'balance' => 0, 'interestRate' => 0.1];
    DB::insert('accounts', $data);
    $log->debug(sprintf("Account created", DB::insertId(), $_SESSION['user']['id'], $_SERVER['REMOTE_ADDR']));
    return $this->view->render($response, 'createaccount_success.html.twig');
});
