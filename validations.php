<?php

require_once 'vendor/autoload.php';
require_once 'init.php';

function validateName($name) {
    if (preg_match('/^[a-zA-Z0-9 ,\.-]{2,100}$/', $name) !== 1) {        
         return "Name must be 2 to 100 characters long made up of letters, digits, space, comma, dot, dash";
    }
    return true;
}

function validateAddress($address) {
    if (preg_match('/^[a-zA-Z0-9 ,\.-]{2,100}$/', $address) !== 1) {
        return "Address must be 2 to 100 characters long made up of letters, digits, space, comma, dot, dash";
    }
    return true;
}

function validateEmail($email, $id = null) {
if (filter_var($email, FILTER_VALIDATE_EMAIL) === false) {
        return "Email must resemble an email format";
    } else {
        if ($id == null) { return true; }
        $client = DB::query("SELECT * FROM clients WHERE email=%s AND id != %d", $email, $id);
        if ($client) {
            return "Email is already in use ";
        }
    }
    return true;
}

function validatePhone($phone) {
    if (strlen($phone) < 7 || strlen($phone) > 20) {
        return "Phone must be 7 to 20 numbers long";
    }
    return true;
}

function validatePassword($password, $password2) {
    if ($password != $password2) {
        return "Passwords do not match";
    } else {
        if (strlen($password) < 6 || strlen($password) > 100
                || (preg_match("/[A-Z]/", $password) == FALSE )
                || (preg_match("/[a-z]/", $password) == FALSE )
                || (preg_match("/[0-9]/", $password) == FALSE )) {
            return "Password must be 6-100 characters long, with at least one uppercase, one lowercase, and one digit in it"; 
        }
    }
    return true;
}