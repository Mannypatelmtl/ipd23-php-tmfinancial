<?php

require_once 'vendor/autoload.php';

require_once 'init.php';

// STATE 1: first display of the form
$app->get('/accountslist', function ($request, $response, $args) {
    $clientId = $_SESSION['user']['id'];
    $accountList = DB::query("SELECT * FROM accounts where clientId=%d", $clientId);
    return $this->view->render($response, 'listaccounts.html.twig', ['list' => $accountList, 'clientId' => $clientId]);
});


