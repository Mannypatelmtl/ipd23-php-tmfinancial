<?php

require_once 'vendor/autoload.php';

require_once 'init.php';

// STATE 1: first display of the form
$app->get('/translist/{id:[0-9]+}', function ($request, $response, $args) {
    $transList = DB::query("SELECT * FROM transactions where fromAccount=%d OR toAccount=%d", $args['id'],$args['id']);
    return $this->view->render($response, 'listtrans.html.twig', ['list' => $transList, 'accountId' => $args['id']] );
});