<?php

require_once 'vendor/autoload.php';

require_once 'init.php';
// STATE 1: first display
$app->get('/', function ($request, $response, $args) {
    return $this->view->render($response, 'index.html.twig');
});


// STATE 1: first display
$app->get('/login', function ($request, $response, $args) {
    return $this->view->render($response, 'login.html.twig');
});

// STATE 2&3: receiving submission
$app->post('/login', function ($request, $response, $args) use ($log) {
    $email = $request->getParam('email');
    $password = $request->getParam('password');
    //
    $record = DB::queryFirstRow("SELECT * FROM clients WHERE email=%s", $email);
    $loginSuccess = false;
    if ($record) {
        global $passwordPepper;
        $pwdPeppered = hash_hmac("sha256", $password, $passwordPepper);
        $pwdHashed = $record['password'];
        if (password_verify($pwdPeppered, $pwdHashed)) {
            $loginSuccess = true;
        }
        // WARNING: only temporary solution to allow for old plain-text passwords to continue to work
        // Plain text passwords comparison
        else if ($record['password'] == $password) {
            $loginSuccess = true;
        }
    }
    //
    if (!$loginSuccess) {
        $log->info(sprintf("Login failed for email %s from %s", $email, $_SERVER['REMOTE_ADDR']));
        return $this->view->render($response, 'login.html.twig', [ 'error' => true ]);
    } else {
        unset($record['password']); // for security reasons remove password from session
        $_SESSION['user'] = $record; // remember user logged in
        $isAdmin = $_SESSION['user']['isAdmin'];
        $log->debug(sprintf("Login successful for email %s, uid=%d, from %s", $email, $record['id'], $_SERVER['REMOTE_ADDR']));
        return $this->view->render($response, 'login_success.html.twig', ['userSession' => $_SESSION['user'], 'isAdmin' =>$isAdmin ] );
    }
});

$app->get('/logout', function ($request, $response, $args) use ($log) {
    $log->debug(sprintf("Logout successful for uid=%d, from %s", @$_SESSION['user']['id'], $_SERVER['REMOTE_ADDR']));
    unset($_SESSION['user']);
    return $this->view->render($response, 'logout.html.twig', ['userSession' => null ]);
});


// STATE 1: first display of the form
$app->get('/newclient', function ($request, $response, $args) {
    return $this->view->render($response, 'newclient.html.twig');
});


// STATE 2&3: receiving submission
$app->post('/newclient', function ($request, $response, $args) use ($log) {
    $name = $request->getParam('name');
    $address = $request->getParam('address');
    $email = $request->getParam('email');
    $phone = $request->getParam('phone');
    $dob = $request->getParam('dob');
    $password = $request->getParam('password');
    $password2 = $request->getParam('password2');
    //
    $errorList = [];
    if (strlen($name) < 2 || strlen($name) > 100) {
        $errorList[] = "Name must be 2-100 characters long";
    }
    if (strlen($address) < 2 || strlen($name) > 100) {
        $errorList[] = "Address must be 2-100 characters long";
    }
    if (filter_var($email, FILTER_VALIDATE_EMAIL) === false) {
        $errorList[] = "email must look like an email";
    } else {
         // is email already in use?
      $client = DB::queryFirstRow("SELECT * FROM clients WHERE email=%s", $email);
      if ($client) {
          $errorList[] = "This email is already registered";
        }
    }
    if (strlen($phone) < 7 || strlen($phone) > 20) {
        $errorList[] = "Phone must be 7-20 characters long";
    }
    if ($password != $password2) {
        $errorList[] = "Passwords do not match";
    } else {
        // maybe? preg_match("/^(?=.*[0-9])(?=.*[A-Z])(?=.*[a-z])[a-zA-Z0-9]{6,100}$/"
        if (strlen($password) < 6 || strlen($password) > 100
            || (preg_match("/[A-Z]/", $password) == FALSE )
            || (preg_match("/[a-z]/", $password) == FALSE )
            || (preg_match("/[0-9]/", $password) == FALSE )) {
                $errorList[] = "Password must be 6-100 characters long, "
                    . "with at least one uppercase, one lowercase, and one digit in it";
        }
    }
   
    //
    $valuesList = ['name' => $name, 
                   'address' => $address, 
                   'email' => $email, 
                    'phone' => $phone, 
                    'dob' => $dob, 
                    'password' => $password];
    if ($errorList) { // STATE 2: errors - redisplay the form
        return $this->view->render($response, 'newclient.html.twig', ['errorList' => $errorList, 'v' => $valuesList]);
    } else { // STATE 3: success
        DB::insert('clients', $valuesList);
        $log->debug(sprintf("New client profile created  with Id=%s", DB::insertId()));
        return $this->view->render($response, 'newclient_success.html.twig');
    }
});